#include "cliente.hpp"
#include <iostream>
#include <string>

using namespace std;

Cliente::Cliente(){
	set_nome("");
        set_cpf("");
        set_email("");
        set_telefone("");
	define_socio(0);
}

Cliente::Cliente(string nome, string cpf, string email, string telefone){
        set_nome(nome);
        set_cpf(cpf);
        set_email(email);
        set_telefone(telefone);
        define_socio(0);
}


Cliente::Cliente(string nome, string cpf, string email, string telefone, int socio){
	set_nome(nome);
	set_cpf(cpf);
	set_email(email);
	set_telefone(telefone);
	define_socio(socio);
}
       

Cliente:: ~Cliente(){}

void Cliente::define_socio(int socio){
	this->socio = socio;
}

int Cliente::get_socio(){
	return socio;
}

void Cliente::imprime_dados(){
        cout << "Nome: " << get_nome() << endl;
        cout << "CPF: " << get_cpf() << endl;
        cout << "Email: " << get_email() << endl;
        cout << "Telefone: " << get_telefone() << endl;
}

