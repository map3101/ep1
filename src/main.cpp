#include "pessoa.hpp"
#include "cliente.hpp"
#include "estoque.hpp"
#include "compra.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

string getCPF(){
    string valor;
    getline(cin, valor);
    while(valor.size() != 11){
	cout << "CPF inválido, digite novamente: ";
	getline(cin, valor);
    }	
    return valor;
}

int tornar_socio(char admp){
	if(admp == 's'){return 1;}
        if(admp == 'n'){return 2;}
        else{cout << "Resposta inválida, por favor digite novamente: ";cin >> admp; tornar_socio(admp);}
}


string getString(){
    string valor;
    getline(cin, valor);
    return valor;
}


template <typename T1>

T1 getInput(){
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}

int main(int argc, char ** argv){
	
	vector<Estoque * > carrinho;

	int tinicial = -1;
	
	while(tinicial != 0){
	cout << "(1) Modo Venda" << endl << "(2) Gerenciamento do estoque" << endl << "(3) Recomendações de produtos" << endl << "(0) Sair do programa" << endl;
	tinicial = getInput<int>();
		
	switch(tinicial){
		case 1:
		{ 
			string cpf;
			string nome;
			string email;
			string telefone;
			char qsoc;
			int socio;
			cout << "===============" << endl;
			cout << "  Modo Venda" << endl << "===============" << endl;
			cout << "Insira o CPF do cliente para verificação de cadastro: ";
			cpf = getCPF();	
			Cliente * customer;
			customer = new Cliente();						
			ifstream cliente;
		        cliente.open("clientes/" + cpf + ".txt");
			if(cliente.is_open() == false){                    
				ofstream cliente("clientes/" + cpf + ".txt");
				cout << "Cliente não cadastrado. Por favor, insira os dados do cliente." << endl;
				cout << "Nome: ";
				nome = getString();
				cliente << nome << endl;
				cliente << cpf << endl;
				cout << "E-mail: ";
				email = getString();
				cliente << email << endl;	
				cout << "Telefone: ";
				telefone = getString();
				cliente << telefone << endl;
				cout << "O cliente deseja se tornar sócio? [s/n]";
				qsoc = getInput<char>();
				int ref = 0;
				while(ref == 0){
					if(qsoc == 's'){socio = 1; cliente << socio; ref++; break;}
					if(qsoc == 'n'){socio = 0; cliente << socio; ref++; break;}
					else{cout << "Resposta inválida, por favor digite novamente: ";qsoc = getInput<char>();}
				}
				cout << "Cadastro realizado com sucesso." << endl;
				cliente.close();
				customer->set_nome(nome);
				customer->set_cpf(cpf);
				customer->set_email(email);
				customer->set_telefone(telefone);
				customer->define_socio(socio);
			}

			if(cliente.is_open()){
				int cont = 1;
				string linha;
 				while(!cliente.eof()){
					getline(cliente,linha);
					if(cont == 1){customer->set_nome(linha);}
                                        if(cont == 2){customer->set_cpf(linha);}
                                        if(cont == 3){customer->set_email(linha);}
                                        if(cont == 4){customer->set_telefone(linha);}
                                        if(cont == 5){int i; i = stoi(linha); customer->define_socio(i);}
                                        cont++;
				}
				cliente.close();
			}
			
			if(customer->get_socio()==1){cout << "O cliente é socio" << endl;}
			if(customer->get_socio()==0){cout << "O cliente não é socio. Gostaria de se tornar um sócio agora? [s/n]" << endl;
				char r;
				r = getInput<char>();
				tornar_socio(r);
				if(tornar_socio(r)==1){customer->define_socio(1);}
			}
			Compra compra(customer);
			break;
			}//case 1 switch principal (modo venda)										
			
		case 2:	
		{
			int com = -1;
			cout << "========================" << endl;
			cout << "Gerenciamento do estoque" << endl;
			cout << "========================" << endl;
			while(com != 0){
			cout << "(1) Adicionar produto" << endl << "(2) Alterar quantidade de um produto" << endl << "(3) Verificar a quantidade de um produto." << endl << "(4) Alterar o preço de um produto" << endl << "(5) Verificar o preço de um produto" << endl << "(0) Voltar ao menu" << endl;
                        com = getInput<int>();           
				
			switch(com){
				case 1:
	                        {              	
					Estoque * produto;
					produto = new Estoque();
					produto->grava_Produto();        
                       			break;	
				}//case 1 modo estoque					
                        	case 0:
					{
						break;
					}//case 0 modo estoque
			
				case 2:
					{
						string nomeproduto;
						int quantidadeproduto;
                                        	cout << "Digite o nome do produto: ";
                                        	nomeproduto = getString();

                                        	ifstream produto;
                                        	produto.open("produtos/" + nomeproduto + ".txt");
						
						if(!produto.is_open()){
                                                        cout << "Produto não encontrado no estoque." << endl;
                                                        break;
                                                }


						if(produto.is_open()){
							Estoque * product;
							product = new Estoque();
							int cont = 1;
							string linha;
							while(!produto.eof()){
								getline(produto,linha);
								
								if(cont == 1){int i; i = stoi(linha); product->set_id(i);}
								if(cont == 2){product->set_produto(linha);}
								if(cont == 3){product->set_categoria(linha);}
								if(cont == 4){int i; i = stoi(linha); product->set_quantidade(i);}
								if(cont == 5){float i; i = stof(linha); product->set_preco(i);}
							cont++;}
									
							cout << "Digite a nova quantidade: ";
							quantidadeproduto = getInput<int>();
							product->set_quantidade(quantidadeproduto);
							
							ofstream produto("produtos/" + nomeproduto + ".txt");
							produto << product->get_id() << endl << product->get_produto() << endl << product->get_categoria() << endl << product->get_quantidade() << endl << product->get_preco() << endl;
							delete product;
							produto.close();
						}
						break;		
					
					}//case 2 modo estoque
				case 3:
					{
						string nomeproduto;
                                                cout << "Digite o nome do produto: ";
                                                nomeproduto = getString();

                                                ifstream produto;
                                                produto.open("produtos/" + nomeproduto + ".txt");
						
						if(produto.is_open() == false){
                                                        cout << "Produto não encontrado no estoque." << endl;
                                                        break;
                                                }


                                                if(produto.is_open()){
							Estoque * product;
							product = new Estoque();
							int cont = 1;
							string linha; 
							while(!produto.eof()){
                                                                getline(produto,linha);
								if(cont == 1){int i; i = stoi(linha); product->set_id(i);}
                                                                if(cont == 2){product->set_produto(linha);}
                                                                if(cont == 3){product->set_categoria(linha);}
                                                                if(cont == 4){int i; i = stoi(linha); product->set_quantidade(i);}
                                                                if(cont == 5){float i; i = stof(linha); product->set_preco(i);}

                                                        cont++;}
							cout << "Quantidade do produto " << product->get_produto() << ": " << product->get_quantidade() << endl;
							delete product;
							produto.close();
							break;
						}									
						
						
					}//case 3 modo estoque
					
					case 4:
						{
							string nomeproduto;
							float precoproduto;
                                                	cout << "Digite o nome do produto: ";
                                                	nomeproduto = getString();

                                                	ifstream produto;
                                                	produto.open("produtos/" + nomeproduto + ".txt");

                                                	if(produto.is_open() == false){
                                                        cout << "Produto não encontrado no estoque." << endl;
                                                        break;
                                                }


                                                	if(produto.is_open()){
                                                        	Estoque * product;
                                                        	product = new Estoque();
                                                        	int cont = 1;
                                                        	string linha;
                                                        	while(!produto.eof()){
                                                                	getline(produto,linha);
                                                                	if(cont == 1){int i; i = stoi(linha); product->set_id(i);}
                                                                	if(cont == 2){product->set_produto(linha);}
                                                                	if(cont == 3){product->set_categoria(linha);}
                                                                	if(cont == 4){int i; i = stoi(linha); product->set_quantidade(i);}
                                                                	if(cont == 5){float i; i = stof(linha); product->set_preco(i);}

                                                        	cont++;}
                                                        	cout << "Digite o novo preço: ";
								precoproduto = getInput<float>();
								product->set_preco(precoproduto);
                                                        	ofstream produto("produtos/" + nomeproduto + ".txt");
                                                        	produto << product->get_id() << endl << product->get_produto() << endl << product->get_categoria() << endl << product->get_quantidade() << endl << product->get_preco() << endl;
                                                        	delete product;
                                                        	produto.close();
								break;
							}
						}//case 4 switch modo estoque 
					
					case 5:
						{
							string nomeproduto;
                                                	cout << "Digite o nome do produto: ";
                                                	nomeproduto = getString();

                                                	ifstream produto;
                                                	produto.open("produtos/" + nomeproduto + ".txt");

                                                	if(produto.is_open() == false){
                                                        	cout << "Produto não encontrado no estoque." << endl;
                                                        	break;
                                                	}


                                                	if(produto.is_open()){
                                                        	Estoque * product;
                                                        	product = new Estoque();
                                                        	int cont = 1;
                                                        	string linha;
                                                        	while(!produto.eof()){
                                                                	getline(produto,linha);
                                                                	if(cont == 1){int i; i = stoi(linha); product->set_id(i);}
                                                                	if(cont == 2){product->set_produto(linha);}
                                                                	if(cont == 3){product->set_categoria(linha);}
                                                                	if(cont == 4){int i; i = stoi(linha); product->set_quantidade(i);}
                                                                	if(cont == 5){float i; i = stof(linha); product->set_preco(i);}

                                                        	cont++;}
                                                        	cout << "Preço do produto " << product->get_produto() << ": " << product->get_preco() << endl;
                                                        	delete product;
                                                        	produto.close();
                                                        	break;
                                                }


						}//case 5 switch modo estoque
				default:{ cout << "Entrada inválida!" << endl;}
			}//switch case modo estoque		
			}//while modo estoque

		break;
		}//case 2 switch principal
			

			
		case 3:
		{
			cout << "Digite o cpf do cliente: ";
			string cpf;
			cpf = getCPF();
                        Cliente * customer;
                        customer = new Cliente();
                        ifstream cliente;
                        cliente.open("clientes/" + cpf + ".txt");
                        if(cliente.is_open()){
                        	int cont = 1;
                                string linha;
                                while(!cliente.eof()){
                                        getline(cliente,linha);
                                        if(cont == 1){customer->set_nome(linha);}
                                        if(cont == 2){customer->set_cpf(linha);}
                                        if(cont == 3){customer->set_email(linha);}
                                        if(cont == 4){customer->set_telefone(linha);}
                                        if(cont == 5){int i; i = stoi(linha); customer->define_socio(i);}
                                        cont++;
                                }
                                cliente.close();
        
			}
			
			int w = 1;
			Compra imprimir(customer, w); 
			
		
		}

		default:{cout << "Entrada inválida!\n" << endl;}
	}//switch princlipal




	}//while principal
return 0;	
}
