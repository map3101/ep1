#include "estoque.hpp"

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string getText(){
    string valor;
    getline(cin, valor);
    return valor;
}


template <typename T1>

T1 getInput(){
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}


Estoque::Estoque(){
	set_id(0);
	set_produto("");
	set_categoria("");
	set_quantidade(0);
	set_preco(0.00f);
}

Estoque::Estoque(int id, string produto, string categoria, int quantidade, float preco){
	set_id(id);
	set_produto(produto);
	set_categoria(categoria);
	set_quantidade(quantidade);
	set_preco(preco);
}

Estoque::~Estoque(){}

void Estoque::set_produto(string produto){
	this->produto = produto;
}

string Estoque::get_produto(){
	return produto;
}

void Estoque::set_categoria(string categoria){
	this->categoria = categoria;
}

string Estoque::get_categoria(){
	return categoria;
}

void Estoque::set_quantidade(int quantidade){
	this->quantidade = quantidade;
}

int Estoque::get_quantidade(){
	return quantidade;
}

void Estoque::set_id(int id){
	this->id = id;
}

int Estoque::get_id(){
	return id;
}

void Estoque::set_preco(float preco){
	this->preco = preco;
}

float Estoque::get_preco(){
	return preco;
}

void Estoque::grava_Produto(){
	 ifstream idprod("produtos/id.txt");
         	int id;
                string linha;
                getline(idprod,linha);
                id = stoi(linha);
                idprod.close();
                string nomeproduto;
                string categoriaproduto;
                int quantidadeproduto;
                float preco;
                cout << "Digite o nome do produto: ";
                nomeproduto = getText();
                ifstream produto;
                produto.open("produtos/" + nomeproduto + ".txt");
                if(produto.is_open()){
                	cout << "Produto já existe no estoque." << endl;
                }
                else{
                	ofstream produto("produtos/" + nomeproduto + ".txt");
                        cout << "Digite a categoria do produto: ";
                        categoriaproduto = getText();
                        produto << id << endl;
                        produto << nomeproduto << endl;
                        produto << categoriaproduto << endl;
                        cout << "Digite a quantidade incial: ";
                        quantidadeproduto = getInput<int>();
                        produto << quantidadeproduto << endl;
                        cout << "Digite o preço desejado: ";
                        preco = getInput<float>();
                        produto << preco << endl;
                        ofstream idprod("produtos/id.txt");
                        id++;
                        idprod << id;
                        idprod.close();
                }
}

void Estoque::altera_quantidade(string nomeproduto, int quantidade_inicial, int quantidade_compra){
	ifstream produto;
        produto.open("produtos/" + nomeproduto + ".txt");
        if(!produto.is_open()){
	        cout << "Produto não encontrado no estoque." << endl;
        }
        if(produto.is_open()){
	        Estoque * product;
        	product = new Estoque();
                int cont = 1;
                string linha;
                while(!produto.eof()){
                	getline(produto,linha);

                	if(cont == 1){int i; i = stoi(linha); product->set_id(i);}
                  	if(cont == 2){product->set_produto(linha);}
                        if(cont == 3){product->set_categoria(linha);}
                        if(cont == 4){int i; i = stoi(linha); product->set_quantidade(i);}
                        if(cont == 5){float i; i = stof(linha); product->set_preco(i);}
                        cont++;}

                
                quantidade_inicial =  product->get_quantidade();
		int quantidade_atualizada = quantidade_inicial - quantidade_compra;
		product->set_quantidade(quantidade_atualizada);

		ofstream produto("produtos/" + nomeproduto + ".txt");
                produto << product->get_id() << endl << product->get_produto() << endl << product->get_categoria() << endl << product->get_quantidade() << endl << product->get_preco() << endl;
                delete product;
                produto.close();
	}

}
