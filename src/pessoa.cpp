#include "pessoa.hpp"
#include <iostream>
#include <string>

using namespace std;

Pessoa::Pessoa(){
	set_nome("");
	set_cpf("");
	set_email("");
	set_telefone("");
		
}

Pessoa::~Pessoa(){}

void Pessoa::set_nome(string nome){
	this->nome = nome;
}

string Pessoa::get_nome(){
	return nome;
}

void Pessoa::set_cpf(string cpf){
	this->cpf = cpf;
}

string Pessoa::get_cpf(){
	return cpf;
}

void Pessoa::set_email(string email){
	this->email = email;
}

string Pessoa::get_email(){
	return email;
}

void Pessoa::set_telefone(string telefone){
	this->telefone = telefone;
}

string Pessoa::get_telefone(){
	return telefone;
}

void Pessoa::imprime_dados(){
	cout << "Nome: " << get_nome() << endl;
	cout << "CPF: " << get_cpf() << endl;
	cout << "Email: " << get_email() << endl;
	cout << "Telefone: " << get_telefone() << endl;
}
