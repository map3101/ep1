#include "estoque.hpp"
#include "cliente.hpp"
#include "pessoa.hpp"
#include "compra.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int verifica_adicao(char admp){
	if(admp == 's'){return 1;}
	if(admp == 'n'){return 2;}
	else{cout << "Resposta inválida, por favor digite novamente: ";cin >> admp; verifica_adicao(admp);}
}

template <typename T1>

T1 getInput(){
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}

Compra::Compra(Cliente * cliente){
	cout << "-Dados do cliente-\n" <<"Nome: "<<cliente->get_nome() << endl << "CPF: " << cliente->get_cpf() << endl;
	cout << "------------------" << endl;
	verifica_socio(cliente);
	salvacpf(cliente);
	adiciona_produto();
}

Compra::Compra(Cliente * cliente, int w){
        cout << "-Dados do cliente-\n" <<"Nome: "<<cliente->get_nome() << endl << "CPF: " << cliente->get_cpf() << endl;
        cout << "------------------" << endl;
        salvacpf(cliente);
        imprime_cat();
	w = 1;
}

Compra::~Compra(){}

void Compra::verifica_socio(Cliente * cliente){
	this->socio = cliente->get_socio();
}

int Compra::retorna_socio(){
	return socio;
}

void Compra::salvacpf(Cliente * cliente){
	this->ocpf = cliente->get_cpf();
}

string Compra::retorna_cpf(){
	return ocpf;
}

void Compra::setw(int w){
	this->w = 1;
}

int Compra::getw(){
	return w;
}

void Compra::adiciona_produto(){
	cin.clear();
	string nome_produto;
	string categoria;
	int quantidade_estoque;
	float preco;
	int id;
	
	cout << "Digite o nome do produto a ser adicionado: ";
	nome_produto = getInput<string>();

	cout << "A quantidade desejada: ";

	int quantidade_desejada = getInput<int>();
		
	ifstream arquivo;
	string linha;
	int cont = 1;
	arquivo.open("produtos/" + nome_produto + ".txt");
	if(arquivo.is_open()){

		while(!arquivo.eof()){
			getline(arquivo, linha);
			if(cont == 1){id = stoi(linha);}
			if(cont == 3){categoria = linha;}
			if(cont == 4){quantidade_estoque = stoi(linha);}
			if(cont == 5){preco = stof(linha);}
			cont++;
		}
			
		this->quantidade_estoque.push_back(quantidade_estoque);
		this->carrinho.push_back(new Estoque(id, nome_produto, categoria, quantidade_desejada, preco));
		
	}
	else if(arquivo.is_open() == false){cout << "Produto não encontrado no estoque!" << endl; adiciona_produto();}
	cout << "Produto adicionado ao carrinho!" << endl;
	cout << "Adicionar mais produtos? [s/n]" << endl;
	char admp = getInput<char>();
       	int v = verifica_adicao(admp);
	if(v == 1){adiciona_produto();}
	if(v == 2){finaliza_compra();}

}

void Compra::finaliza_compra(){
	cout << "--Lista de produtos no carrinho--" << endl;
	cout << "Produto" << "\tQuantidade" << "\tPreço" << endl;
	float preco_final = 0;
	for(unsigned int i = 0; i < this->carrinho.size(); i++){
		cout << this->carrinho[i]->get_produto() << "\t" << this->carrinho[i]->get_quantidade() << "\t\t" << this->carrinho[i]->get_quantidade()*this->carrinho[i]->get_preco() << endl;
	preco_final+= this->carrinho[i]->get_quantidade()*this->carrinho[i]->get_preco();
	}
	
	cout << "Finalizando compra...\n" << "Checando estoque..." << endl;
	int fim_compra = 0;
	for(unsigned int i = 0; i < this->carrinho.size(); i++){
		if(this->quantidade_estoque[i] < this->carrinho[i]->get_quantidade()){
			cout << "Compra cancelada, quantidade em estoque do produto " << this->carrinho[i]->get_produto() << " insuficiente!"<< endl; fim_compra++;
		}
	}
	
	if(fim_compra == 0){
		if(retorna_socio()==1){
			cout<< "Cliente sócio, desconto de 15%!" << " Valor final: R$ " << preco_final * 0.75 << endl;
		}

		if(retorna_socio()==0){
			cout << "Valor final: R$" << preco_final << endl;
		}
		for(unsigned int k; k < this->carrinho.size(); k++){
			this->carrinho[k]->altera_quantidade(this->carrinho[k]->get_produto(), this->quantidade_estoque[k], this->carrinho[k]->get_quantidade());
			grava_cat(this->carrinho[k]->get_categoria());	
		}
		cout << "Compra finalizada!" << endl << "=========================="<< endl;
	}	
}

void Compra::grava_cat(string gravacao){
        string tcpf;
        tcpf = retorna_socio();
        ofstream lista("listacategorias/" + tcpf + ".txt", ofstream::app);
        lista << gravacao << endl;
}

void Compra::imprime_cat(){
	string tecpf;
        tecpf = retorna_cpf();
	string linha;
	ifstream arquivo("listacategorias/" + tecpf + ".txt");
	if(arquivo.is_open()){
		cout << "Histórico de categorias" << endl;
		while(!arquivo.eof()){
			getline(arquivo, linha);
			cout << linha;
		}
	}	
}


