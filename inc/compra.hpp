#ifndef COMPRA_HPP
#define COMPRA_HPP

#include "estoque.hpp"
#include "pessoa.hpp"
#include "cliente.hpp"

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Compra{
private:
	vector<Estoque * > carrinho;
	vector<int> quantidade_estoque;
	int socio;
	string ocpf;
	int w;

public:
	Compra(Cliente *);
	Compra(Cliente *, int w);
	~Compra();
	void adiciona_produto();
	void finaliza_compra();
	void verifica_socio(Cliente *);
	int retorna_socio();	
	void grava_cat(string gravacao);
	void salvacpf(Cliente *);
	string retorna_cpf();
	void imprime_cat();
	void setw(int w);
	int getw();
};

#endif
