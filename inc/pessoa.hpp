#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>

using namespace std;

class Pessoa {
private:
	string nome;
	string cpf;
	string email;
	string telefone;
		
public:
	Pessoa();
	~Pessoa();

	void set_nome(string nome);
	string get_nome();

	void set_cpf(string cpf);
	string get_cpf();

	void set_email(string email);
	string get_email();
	
	void set_telefone(string telefone);
	string get_telefone();

	void imprime_dados();		
};

#endif
