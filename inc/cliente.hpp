#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include "pessoa.hpp"

class Cliente : public Pessoa{

private:
	int socio;


public:
	Cliente();
	Cliente(string nome, string cpf, string email, string telefone);
	Cliente(string nome, string cpf, string email, string telefone, int socio);
	~Cliente();
	void define_socio(int socio);
	int get_socio();
	void imprime_dados();

};

#endif
