#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP

#include <iostream>
#include <string>

using namespace std;

class Estoque{
private:
	int id;
	string produto;
	string categoria;
	int quantidade;
	float preco;

public:
	Estoque();
	Estoque(int id, string produto, string categoria, int quantidade, float preco);
	~Estoque();
	
	void set_produto(string produto);
	string get_produto();
	
	void set_categoria(string categoria);
	string get_categoria();	
		
	void set_quantidade(int quantidade);
	int get_quantidade();

	void set_id(int id);
	int get_id();

	void set_preco(float preco);
	float get_preco();

	void grava_Produto();
	void altera_quantidade(string nomeproduto, int quantidade_inicial, int quantidade_compra);
};	

#endif
